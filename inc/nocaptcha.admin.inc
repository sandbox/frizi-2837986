<?php

function nocaptcha_admin_settings_form() {
  $form = array();

  $form['captcha_public'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Key'),
    '#description' => t('Enter the Captcha Key.'),
    '#default_value' => variable_get('captcha_public', ''),
    '#maxlength' => 64,
    '#required' => true,
  );

  $form['captcha_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret NoCaptcha key'),
    '#default_value' => variable_get('captcha_secret', ''),
    '#maxlength' => 64,
    '#description' => t('Enter the Secret NoCaptcha Key.'),
    '#required' => true,
  );

  $form['captcha_key'] = array(
    '#markup' => '<p>' . t('Got to !nocaptcha_link for further Details.', array('!nocaptcha_link' => '<a href="https://www.google.com/recaptcha/admin#list" target="_blank">NoCaptcha</a>')) . '</p>',
  );

  //$form['#submit'][] = 'capmod_admin_settings_form_submit';
  return system_settings_form($form);
}
