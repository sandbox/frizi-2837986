<?php

/**
 * Implements hook_webform_component_info().
 */
function nocaptcha_webform_component_info() {
  $components = array();
  $components['nocaptcha'] = array(
    'label' => t('NoCaptcha 5'),
    'description' => t('Add Form Validation.'),
    'file' => 'components/nocaptcha.inc',
    'features' => array(
      'csv' => true,
      'required' => false,
      'title_display' => 'none',
      'title_inline' => false,
      'conditional' => false,
      'group' => true,
    ),
  );
  return $components;
}


/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_nocaptcha() {
  return array(
    'name' => 'NoCaptcha',
    'form_key' => NULL,
    'pid' => 0,
    'element_id' => 'c_nocaptcha',
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'extra' => array(
      'private' => FALSE,
      'description' => '',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_nocaptcha($component) {
  $form = array();
  $form['value'] = array(
    '#type' => 'nocaptcha',
    '#title' => 'NoCaptcha',
    '#title_display' => 'before',
    '#description' => '',
    '#weight' => 0,
  );
  return $form;
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_nocaptcha() {
  return array(
    'webform_display_nocaptcha' => array(
      'render element' => 'element',
    ),
  );
}



/**
 * Implements _webform_display_component().
 */
function _webform_display_nocaptcha($component, $value, $format = 'html') {
  $element = array(
    '#title' => 'NoCaptcha',
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_nocaptcha',
    '#theme_wrappers' => array('webform_element'),
    '#value' => 'NoCaptcha ReCaptcha',
    '#translatable' => array('title'),
  );
  return $element;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_nocaptcha($component, $value = NULL, $filter = TRUE) {
  $element = array(
    '#title' => 'NoCaptcha',
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => $filter ? webform_replace_tokens($component['value']) : $component['value'],
    '#type' => 'nocaptcha',
    '#weight' => $component['weight'],
    '#prefix' => '<div class="webform-component-textfield" id="webform-component-' . $component['form_key'] . '">',
    '#suffix' => '</div>',
  );
  return $element;
}

/**
 * Implements _form_builder_webform_form_builder_types_component().
 */
function _form_builder_webform_form_builder_types_nocaptcha() {
  $fields = array();
  $component['name'] = t('New Captcha');
  $fields['nocaptcha'] = array(
    'title' => t('NoCaptcha'),
    'weight' => 9,
    'default' => _form_builder_webform_default('nocaptcha', array(), $component)
  );
  return $fields;
}

/**
 * Implements _form_builder_webform_form_builder_map_component().
 */
function _form_builder_webform_form_builder_map_nocaptcha() {
  return array(
    'form_builder_type' => 'nocaptcha',
    'properties' => array(),
  );
}

function _form_builder_webform_form_builder_preview_alter_nocaptcha($form_element) {
  $form_element['#children'] = t('@title - <em>No Captcha</em>', array('@title' => $form_element['#webform_component']['name']));
  return $form_element;
}


function nocaptcha_form_form_builder_positions_alter(&$form, &$form_state, $form_id) {
  if(module_exists('form_builder')) {
    $form['#attached']['css'][] =  array(
      'data' => '#form-builder-field-palette ul li.field-nocaptcha {
    background-image: url("/' . drupal_get_path('module', 'form_builder') . '/images/fields/checkboxes.png");
    }',
      'type' => 'inline',
    );
  }
}







